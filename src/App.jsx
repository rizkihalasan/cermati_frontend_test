import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import HighlightsPanel from './components/HighlightsPanel';
import HeroShot from './components/HeroShot';
import NewsletterPanel from './components/NewsletterPanel';
import NotificationPanel from './components/NotificationPanel';
import PageFooter from './components/PageFooter';

class App extends Component {
  render() {
    return (
      // <div className="App">
      //   <header className="App-header">
      //     <img src={logo} className="App-logo" alt="logo" />
      //     <p>
      //       Edit <code>src/App.jsx</code> and save to reload.
      //     </p>
      //     <a
      //       className="App-link"
      //       href="https://reactjs.org"
      //       target="_blank"
      //       rel="noopener noreferrer"
      //     >
      //       Learn React
      //     </a>
      //   </header>
      // </div>
      <div>
        <NotificationPanel />
        <HeroShot />
        <HighlightsPanel />
        <PageFooter />
        <NewsletterPanel />
      </div>
    );
  }
}

export default App;
