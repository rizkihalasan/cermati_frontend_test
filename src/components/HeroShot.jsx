import React, { Component } from 'react';
import './HeroShot.css';

class HeroShot extends Component {
    state = {
        hover:false
    }
    
    mouseOver() {
        this.setState({hover: true});
        console.log('masuk mouse over');
    }

    mouseOut() {
        this.setState({hover: false});
        console.log('masuk mouse out');
    }

    render(){
        const buttonClass = this.state.hover ? 
            'button-heroshot-hover' : 'button-heroshot'
        return (
            <div className='hero-shot'
                style={{
                // backgroundColor:'blue',
                
            }}>
                <div style={{
                    height:'100%',
                    width:'100%',
                    backgroundColor:'rgb(0, 74, 117, 0.8)', 
                }}>
                    <img src='y-logo-white.png' alt="Logo"></img>
                    <div className='heroshot-text'>
                        <div className='title-heroshot'>
                            Hello! I'm Rizki Halasan
                        </div>
                        <div className='detail-heroshot-1'>
                            Consult, Design, and Develop Websites
                        </div>
                        <div className='detail-heroshot-2'>
                            Have something great in mind?
                            Feel free to contact me
                            I'll help you to make it happen
                        </div>
                        <button onMouseOut={this.mouseOut.bind(this)} onMouseOver={this.mouseOver.bind(this)} 
                            className={buttonClass}>
                            LET'S MAKE CONTACT
                        </button>
                    </div>
                </div>
                 
            </div>
        )
    }
}

export default HeroShot;