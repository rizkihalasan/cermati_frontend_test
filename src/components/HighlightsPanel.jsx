import React, { Component } from 'react';
import HighlightsDetail from './HighlightsDetail';
import './HighlightsDetail.css';

class HighlightsPanel extends Component {
    constructor(props){
        super(props)
        this.state = {width: props.width}
    }
    render(){
        console.log('width : ', this.state.width)
        return (
            // <div style={{backgroundColor:'grey'}}>
            <div className='highlights-panel-body'>
                <div className='highlights-panel-text'>
                    <div className='highlights-panel-title'>
                        How Can I Help You ? 
                    </div>
                    <div className='highlights-panel-detail'>
                        Our work then targeted, best practices outcomes social innovation synergy.
                        Venture philanthropy, revolutionary inclusive policymake relief. User-centered
                        program areas scale.
                    </div>
                </div>
                <div className='highlights-panel-container'>
                    <HighlightsDetail
                        title='Consult'
                        description='Co-create, design thinking; strengthen infrastructure resist granular.
                        Revolution circular, movements or framework social impact low-hanging fruit. 
                        Save the world compelling revolutionary progress.'
                        image='fa fa-comments'
                    />
                    <HighlightsDetail
                        title='Design'
                        description='Policymaker collaborates collective impact humanitarian shared value
                        vocabulary inspire issue outcomes agile. Overcome injustice deep dive agile 
                        issue outcomes vibrant boots on the ground sustainable.'
                        image='fa fa-paint-brush'
                    />
                    <HighlightsDetail
                        title='Develop'
                        description='Revolutionary circular, movements a or impact framework social impact low-
                        hanging. Save the compelling revolutionary inspire progress. Collective
                        impacts and challenges for opportunities of thought provoking.'
                        image='fas fa-cubes'
                    />
                    <HighlightsDetail
                        title='Marketing'
                        description='Peaceful; vibrant paradigm, collaborative cities. Shared vocabulary agile,
                        replicable, effective altruism youth. Mobilize commitment to overcome
                        injustice resilient, uplift social transparent effective.'
                        image='fas fa-bullhorn'
                    />
                    <HighlightsDetail
                        title='Manage'
                        description='Change-makers innovation or shared unit of analysis. Overcome injustice
                        outcomes strategize vibrant boots on the ground sustainable. Optimism,
                        effective altruism invest optimism corporate social.'
                        image='fas fa-tasks-alt'
                    />
                    <HighlightsDetail
                        title='Evolve'
                        description='Activate catalyze and impact contextualize humanitarian. Unit of analysis
                        overcome injustice storytelling altruism. Thought leadership mass 
                        incarceration. Outcomes big data, fairness, social game-changer.'
                        image='fas fa-chart-line'
                    />
                </div>
            </div>
        )
    }
}

export default HighlightsPanel;