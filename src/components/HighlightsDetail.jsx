import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './HighlightsPanel.css';
import 'font-awesome/css/font-awesome.min.css';


class HighlightsDetail extends Component {
    static propTypes = {
        title: PropTypes.string,
        description: PropTypes.string,
        image: PropTypes.string,
    }

    static defaultProps = {
        title: '',
        description: '',
        image: '',
    }

    constructor(props){
        super(props);
    }

    render(){
        
        return (
            // <div style={{backgroundColor:'grey', borderColor:'black'}}>
            <div className='highlights-detail-body'>
                <div className='highlights-detail-title'>
                    {this.props.title}
                </div>
                <div className='highlights-detail-image'>
                    <i className={this.props.image} ></i>
                </div>
                <div className='highlights-detail-description'>
                    {this.props.description}
                </div>
                
            </div>
        )
    }
}

export default HighlightsDetail;