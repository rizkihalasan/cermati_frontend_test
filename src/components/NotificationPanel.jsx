import React, { Component } from 'react';
import './NotificationPanel.css';
class NotificationPanel extends Component {
    state = {
        show: true
    }
    constructor(props){
        super(props);

        
    }

    hidePanel = () => {
        this.setState({ show: false });
    }

    render(){
        const showHide = this.state.show ? 'notification-panel-body' : 'notification-panel-none';
        return (
            
            <div className={showHide}>
                <div style={{}}>
                    by accessing and using this website, you acknowledge 
                    that you have read and understand our Cookie Policy,
                    Privacy Policy and our Terms of Service
                </div>
                <button className='notification-panel-button' onClick={this.hidePanel}>
                    Got it
                </button>
            </div>
        )
    }
}

export default NotificationPanel;