import React, { Component } from 'react';
import './PageFooter.css';

class PageFooter extends Component {
    render(){
        
        return (
            <div className="page-footer">
                &#9400; 2019 Rizki Halasan. All rights reserved.
            </div>
        )
    }
}

export default PageFooter;