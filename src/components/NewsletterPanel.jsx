import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './NewsletterPanel.css';

class NewsletterPanel extends Component {
    // static propTypes = {
    //     show: PropTypes.bool,
    // }

    // static defaultProps = {
    //     show: true
    // }
    state = {
        show: true
    }
    constructor(props){
        super(props);

        
    }

    hideModal = () => {
        this.setState({ show: false });
    }

    render(){
        const showHide = this.state.show ? 'modal display' : 'modal display-none';
        return (
            <div className={showHide}>
                <div className='modal-title'>
                    Get latest updates in web technologies
                </div>
                <div>
                    I write articles related to web technologies, such as design trends,
                    development tools, UI/UX case studies and reviews, and more. Sign
                    up to my newsletter to get them all
                </div>
                <form style={{width:'100%'}}>
                    <input className='modal-form-text' type="text" name="email" placeholder="Email address"/>
                    <input className='modal-form-submit' type="submit" value="count me in"/>
                </form>
                <button className='modal-button' onClick={this.hideModal}>
                    X
                </button>
            </div>
        )
    }
}

export default NewsletterPanel;